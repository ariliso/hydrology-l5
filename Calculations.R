
# Aris boilerplate  ------------------------------------------------
if (!require("pacman")) install.packages("pacman")
pacman::p_load("data.table", "ggrepel", "ggpmisc", "magrittr", "ggplot2")

if("datasets" %in% (.packages())){
  detach("package:datasets",unload = TRUE) #removes datasets package to avoid variable conflicts
}

## Data Import -------------------------------------------------------------
A_SPLIT_PT <-2300 #splitting point for pt a
a_ele_prec <- fread(file = "pt_a_ele_prec.csv") # Elevation (m) and Precipitation (cm)
a_area_ele <- fread(file = "pt_a_area_ele.csv") # Area (km^2) elevation (m)
bc_dat <- fread(file = "pt_b_c_data.csv")# Hour,Ta(C),RH(%),u(cm/s),Ts(C),Rn(MJ/m^2/h)

# A.1 --------
a_1_plot <-
  ggplot(a_ele_prec, aes(x = ele, y=prec)) +
  geom_point(data = a_ele_prec[ele<A_SPLIT_PT],color = "red") +
  geom_point(data = a_ele_prec[ele>=A_SPLIT_PT],color="blue") +
  stat_smooth(data = a_ele_prec[ele<A_SPLIT_PT],color = "red",method = "lm") +
  stat_smooth(data = a_ele_prec[ele>=A_SPLIT_PT],color = "blue",method = "lm") +
  #    geom_vline(xintercept = A_SPLIT_PT) +
  labs(y = "Precipitation (mm)", x = "Elevation (m)")
a_1_plot


a_1_lm_low <- lm(prec ~ ele,a_ele_prec[ele<A_SPLIT_PT]) #linear model: precipitation varies w/ elevation
a_1_lm_high <- lm(prec ~ ele,a_ele_prec[ele>=A_SPLIT_PT]) # linearmodels are split at split point

a_area_ele[,pr_area := area/sum(area)] #areas as a fraction of their total sum

#the following two lines use the coefficients from appropriate linear model from A.1 to calculate 
# predicted precipitation values by altitude
#the format is as follows [Where elevation < split point, calculated value is ax+b] etc. the library i use
#chains multiple expressions of the form [WHERE,DO] to be chained when placed next to eachother like [][]
#I limited my use of this form for readability but it made sense here *line break is after opening the 2nd expression

a_area_ele[ele<A_SPLIT_PT, predprec := 
             (ele*a_1_lm_low$coefficients[2] + a_1_lm_low$coefficients[1])
           ][ele>=A_SPLIT_PT, predprec := 
             (ele*a_1_lm_high$coefficients[2] + a_1_lm_high$coefficients[1])] 

a_area_ele[,wt_prec := pr_area*predprec] # predicted precipitation weighted by area at altitude

a_3_wtprec <- a_area_ele[,sum(wt_prec)] #weighted precipitation (mm)



a_3_wtprec #average precipitation predicted by altitude linear models (mm) weighted by proportion of area at total altitude 

bc_dat[,es_s := 0.6108*exp(17.27*Ts/(237.3+Ts))][ #calculating saturation vapour pressure at surface temp (kPa)
  ,es_a := 0.6108*exp(17.27*Ta/(237.3+Ta))][
  ,ea_a := es_a*RH/100][ # also in kpa
  ,u := u*3600*24/100/1000][ #converting wind speed units from cm/s to km/d
  ,fu := u*1.46e-4][ #calculating f(u)
  ,E := fu*(es_s-ea_a)*10] #calculating evaporation(cm/day) (multiplying by 10 to convert kPa to mb) 

b_2_plot_E <-
  ggplot(bc_dat,aes(x=Hr,y=E)) +
  geom_point() + geom_line() +
  labs(y = "Evaporation (cm/day)",
       x = "Hour") +
  scale_x_continuous(limits = c(1,24),breaks = c(1,6,12,18,24))

b_2_E_average <- bc_dat[,mean(E)]


b_2_E_average #daily PET (cm or cm/day) (from mean of hourly mass transfer)


ALP <- 1.26 # moisture based regime correction
GAM <- 0.067 #psychometric constant (kPa / C)
RHO <- 1000


bc_dat[ #using saturated air temp calculated above
  ,del := 4098*es_a/(237.3 + Ta)^2][          #calculating delta (kPa/C)
  ,pet := ALP*del/(del+GAM)*Rn*1000/(RHO*GAM)] #potential evapotranspiration (mm/day)

b_2_daily_PET <- bc_dat[,mean(pet)]#daily potential evapotranspiration (mm/day)
